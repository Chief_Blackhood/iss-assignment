import pygame
import random
import math
import time
from pygame import mixer
from configparser import ConfigParser

parser = ConfigParser()
parser.read("config.ini")

pygame.init()
width = 1200
height = 1000
player_state = 1
cnt_time = 900
cnt_level = [0, 0]
level = [1, 1]
sco = pygame.font.Font(parser.get('Fonts', 'font'), 32)
sco2 = pygame.font.Font(parser.get('Fonts', 'font'), 32)
level1 = pygame.font.Font(parser.get('Fonts', 'font'), 32)
level2 = pygame.font.Font(parser.get('Fonts', 'font'), 32)
loss = pygame.font.Font(parser.get('Fonts', 'font'), 64)
loss = loss.render(eval(parser.get('messages', 'crash')), True,
                   (eval(parser.get('color', 'white'))))
next_ = pygame.font.Font(parser.get('Fonts', 'font'), 64)
next_ = next_.render(eval(parser.get('messages', 'next_level')), True,
                     eval(parser.get('color', 'white')))
next_level = pygame.font.Font(parser.get('Fonts', 'font'), 56)
next_level_ = pygame.font.Font(parser.get('Fonts', 'font'), 32)
win = pygame.font.Font(parser.get('Fonts', 'font'), 56)
pause_space = pygame.font.Font(parser.get('Fonts', 'font'), 56)
pause_text = pygame.font.Font(parser.get('Fonts', 'font'), 72)
timer = pygame.font.Font(parser.get('Fonts', 'font'), 32)

pixel_width = 55
no_of_p = 6
score_value = 0
score_value2 = 0

# background music
mixer.music.load("background.mp3")
mixer.music.play(-1)

screen = pygame.display.set_mode((width, height))
done = False

clock = pygame.time.Clock()

pygame.display.set_caption("Crossyrivers")

icon = pygame.image.load('logo.png')
pygame.display.set_icon(icon)

player_img = pygame.image.load("player.png")
player_x = width / 2 - 20
player_y = height
player_x_change = 0
player_y_change = 0

flag = []
for i in range(9):
    flag.append(0)

pla_x = [6, 6]
x = 10
y = 10
ship_img = []
ship_x = []
ship_y = []
ship_x_change = []
num_of_ships = 10

for i in range(num_of_ships):
    ship_img.append(pygame.image.load("ship2.png"))
    if i % 2 == 0:
        ship_x.append(random.randint(60, 936 / 2))
        ship_x_change.append(-x)
    else:
        ship_x.append(random.randint(936 / 2, 936))
        ship_x_change.append(x)
ship_y.append(height - 180)
ship_y.append(height - 180)
ship_y.append(height - 370)
ship_y.append(height - 370)
ship_y.append(height - 560)
ship_y.append(height - 560)
ship_y.append(height - 750)
ship_y.append(height - 750)
ship_y.append(height - 940)
ship_y.append(height - 940)

tree_img = []
tree_x = []
tree_y = []
num_of_trees = 8
for i in range(num_of_trees):
    tree_img.append(pygame.image.load("tree.png"))
    if i % 2 == 0:
        tree_x.append(random.randint(60, 936 / 2))
    else:
        tree_x.append(random.randint(936 / 2, 936))
tree_y.append(height - 250)
tree_y.append(height - 250)
tree_y.append(height - 440)
tree_y.append(height - 440)
tree_y.append(height - 630)
tree_y.append(height - 630)
tree_y.append(height - 820)
tree_y.append(height - 820)

ball_img = []
ball_x = []
ball_y = []
num_of_balls = 4
for i in range(num_of_balls):
    ball_img.append(pygame.image.load("ball.png"))
    ball_x.append(random.randint(60, 936))
ball_y.append(height - 250)
ball_y.append(height - 440)
ball_y.append(height - 630)
ball_y.append(height - 820)


# functions
def show_score():
    global player_state
    score = sco.render("Player 1 : " + str(score_value), True,
                       eval(parser.get('color', 'white')))
    screen.blit(score, (10, 10))
    score2 = sco2.render("Player 2 : " + str(score_value2), True,
                         eval(parser.get('color', 'white')))
    screen.blit(score2, (970, 10))
    time_ = timer.render("Time : " + str(int(cnt_time / 30)), True,
                         eval(parser.get('color', 'white')))
    if player_state == 1:
        player_level = level1.render("Player 1", True,
                                     eval(parser.get('color', 'white')))
        screen.blit(player_level, (10, 960))
        next_level = next_level_.render(
            "Level: " + str(level[player_state - 1]), True,
            eval(parser.get('color', 'white')))
        screen.blit(next_level, (990, 960))
        screen.blit(time_, (width / 2 - 30, 10))

    else:
        player_level_ = level2.render("Player 2", True,
                                      eval(parser.get('color', 'white')))
        screen.blit(player_level_, (10, 960))
        next_level = next_level_.render(
            "Level: " + str(level[player_state - 1]), True,
            eval(parser.get('color', 'white')))
        screen.blit(next_level, (980, 960))
        screen.blit(time_, (width / 2 - 0, height - 40))


def player(x, y):
    screen.blit(player_img, (x, y))


def tree(x, y, i):
    screen.blit(tree_img[i], (x, y))


def ball(x, y, i):
    screen.blit(ball_img[i], (x, y))


def ship(x, y, i):
    screen.blit(ship_img[i], (x, y))


def is_collision_for_tree(tree_x, tree_y, player_x, player_y):
    distance = math.sqrt(
        math.pow(tree_x - player_x, 2) + math.pow(tree_y - player_y, 2))
    if distance < 48:
        return True
    return False


def is_collision_for_ball(ball_x, ball_y, player_x, player_y):
    distance = math.sqrt(
        math.pow(ball_x - player_x, 2) + math.pow(ball_y - player_y, 2))
    if distance < 55:
        return True
    return False


def is_collision_for_ships(ship_x, ship_y, player_x, player_y):
    distance = math.sqrt(
        math.pow(ship_x - player_x, 2) + math.pow(ship_y - player_y, 2))
    if distance < 73:
        return True
    return False


def next_level():
    global cnt_time, y, x, ship_img, ship_x, ship_y, ship_x_change
    global tree_img, tree_x, tree_y, num_of_trees, ball_img, ball_x, ball_y
    global num_of_balls, player_state, flag, cnt_level, pla_x, level
    global num_of_ships, next_level
    screen.fill((0, 0, 0))
    screen.blit(next_, (440, 450))
    next_level = next_level.render("Level: " + str(level[player_state - 1]),
                                  True, (eval(parser.get('color', 'white'))))
    screen.blit(next_level, (490, 515))
    pygame.display.update()
    pygame.time.wait(1000)
    cnt_time = 900
    if cnt_level[player_state - 1] >= 5:
        cnt_level[player_state - 1] = 0
        pla_x[player_state - 1] += 2
    flag = []
    for i in range(9):
        flag.append(0)
    ship_img = []
    ship_x = []
    ship_y = []
    ship_x_change = []
    num_of_ships = 10
    if player_state == 1:
        x += 1
    elif player_state == 2:
        y += 1

    for i in range(num_of_ships):
        ship_img.append(pygame.image.load("ship2.png"))
        if i % 2 == 0:
            ship_x.append(random.randint(60, 936 / 2))
        else:
            ship_x.append(random.randint(936 / 2, 936))
        if player_state == 1 and i % 2 == 0:
            ship_x_change.append(x)
        elif player_state == 1 and i % 2 != 0:
            ship_x_change.append(-x)
        if player_state == 2 and i % 2 == 0:
            ship_x_change.append(y)
        elif player_state == 2 and i % 2 != 0:
            ship_x_change.append(-y)
    ship_y.append(height - 180)
    ship_y.append(height - 180)
    ship_y.append(height - 370)
    ship_y.append(height - 370)
    ship_y.append(height - 560)
    ship_y.append(height - 560)
    ship_y.append(height - 750)
    ship_y.append(height - 750)
    ship_y.append(height - 940)
    ship_y.append(height - 940)

    tree_img = []
    tree_x = []
    tree_yy = []
    num_of_trees = 8
    for i in range(num_of_trees):
        tree_img.append(pygame.image.load("tree.png"))
        if i % 2 == 0:
            tree_x.append(random.randint(60, 936 / 2))
        else:
            tree_x.append(random.randint(936 / 2, 936))
    tree_y.append(height - 250)
    tree_y.append(height - 250)
    tree_y.append(height - 440)
    tree_y.append(height - 440)
    tree_y.append(height - 630)
    tree_y.append(height - 630)
    tree_y.append(height - 820)
    tree_y.append(height - 820)

    ball_img = []
    ball_x = []
    ball_y = []
    num_of_balls = 4
    for i in range(num_of_balls):
        ball_img.append(pygame.image.load("ball.png"))
        ball_x.append(random.randint(60, 936))
    ball_y.append(height - 250)
    ball_y.append(height - 440)
    ball_y.append(height - 630)
    ball_y.append(height - 820)


def score(player_y, player_state):
    global flag, score_value, score_value2
    if player_state == 1:
        if player_y < 748 and flag[0] != 1:
            score_value += 10
            flag[0] = 1
        if player_y < 694 and flag[1] != 1:
            score_value += 5
            flag[1] = 1
        if player_y < 557 and flag[2] != 1:
            score_value += 10
            flag[2] = 1
        if player_y < 503 and flag[3] != 1:
            score_value += 5
            flag[3] = 1
        if player_y < 367 and flag[4] != 1:
            score_value += 10
            flag[4] = 1
        if player_y < 312 and flag[5] != 1:
            score_value += 5
            flag[5] = 1
        if player_y < 178 and flag[6] != 1:
            score_value += 5
            flag[6] = 1
        if player_y < 129 and flag[7] != 1:
            score_value += 5
            flag[7] = 1
        if player_y < 1 and flag[8] != 1:
            score_value += 10
            flag[8] = 1
    elif player_state == 2:
        if player_y > 180 and flag[0] != 1:
            score_value2 += 10
            flag[0] = 1
        if player_y > 240 and flag[1] != 1:
            score_value2 += 5
            flag[1] = 1
        if player_y > 365 and flag[2] != 1:
            score_value2 += 10
            flag[2] = 1
        if player_y > 430 and flag[3] != 1:
            score_value2 += 5
            flag[3] = 1
        if player_y > 555 and flag[4] != 1:
            score_value2 += 10
            flag[4] = 1
        if player_y > 620 and flag[5] != 1:
            score_value2 += 5
            flag[5] = 1
        if player_y > 748 and flag[6] != 1:
            score_value2 += 5
            flag[6] = 1
        if player_y > 805 and flag[7] != 1:
            score_value2 += 5
            flag[7] = 1
        if player_y > 935 and flag[8] != 1:
            score_value2 += 10
            flag[8] = 1


# game loop
while not done:
    if score_value < 0:
        score_value = 0
    if score_value2 < 0:
        score_value2 = 0
    screen.fill((4, 147, 199))

    # for platforms
    for i in range(no_of_p):
        pygame.draw.rect(screen, (237, 201, 175),
                         pygame.Rect(0, i * height / (
                                 no_of_p - 1) - i * pixel_width / (
                                             no_of_p - 1),
                                     width,
                                     pixel_width))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            screen.fill((0, 0, 0))
            if score_value > score_value2:
                who_win = win.render(eval(parser.get('messages', 'Player1')),
                                     True, eval(parser.get('color', 'gold')))
                screen.blit(who_win, (460, 500))
            elif score_value2 > score_value:
                who_win = win.render(eval(parser.get('messages', 'Player2')),
                                     True, eval(parser.get('color', 'gold')))
                screen.blit(who_win, (460, 500))
            else:
                who_win = win.render(eval(parser.get('messages', 'Default')),
                                     True, eval(parser.get('color', 'gold')))
                screen.blit(who_win, (460, 500))
            pygame.display.update()
            pygame.time.delay(1000)
            done = True

        # player movements
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                player_x_change = -pla_x[player_state - 1]
            if event.key == pygame.K_RIGHT:
                player_x_change = pla_x[player_state - 1]
            if event.key == pygame.K_UP:
                player_y_change = -pla_x[player_state - 1]
            if event.key == pygame.K_DOWN:
                player_y_change = pla_x[player_state - 1]
            if event.key == pygame.K_SPACE:
                pause = True
                while pause:
                    surface = pygame.Surface((width, height / 2))
                    surface = surface.convert_alpha()
                    surface.fill((0, 0, 0, 10))
                    screen.blit(surface, (0, 250))
                    pausetext_ = pause_text.render(
                        eval(parser.get('messages', 'Paused')), True,
                        eval(parser.get('color', 'white')))
                    screen.blit(pausetext_, (350, 450))
                    pausespace_ = pause_space.render(
                        eval(parser.get('messages', 'Space')), True,
                        eval(parser.get('color', 'white')))
                    screen.blit(pausespace_, (300, 660))
                    flag_ = 0
                    pygame.display.update()
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            screen.fill((0, 0, 0))
                            if score_value > score_value2:
                                who_win = win.render(
                                    eval(parser.get('messages', 'Player1')),
                                    True, eval(parser.get('color', 'gold')))
                                screen.blit(who_win, (419, 515))
                            elif score_value2 > score_value:
                                who_win = win.render(
                                    eval(parser.get('messages', 'Player2')),
                                    True, eval(parser.get('color', 'gold')))
                                screen.blit(who_win, (419, 515))
                            else:
                                who_win = win.render(
                                    eval(parser.get('messages', 'Default')),
                                    True, eval(parser.get('color', 'gold')))
                                screen.blit(who_win, (419, 515))
                            pygame.display.update()
                            pygame.time.delay(1000)
                            done = True
                            flag_ = 1
                            break
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_SPACE:
                                flag_ = 1
                                mixer.music.unpause()
                                break
                    if flag_ == 1:
                        break
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT \
                    or event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                player_x_change = 0
                player_y_change = 0

    if done:
        break

    player_x += player_x_change
    player_y += player_y_change

    if player_x <= 0:
        player_x = 0
    elif player_x >= 1136:
        player_x = 1136
    if player_y <= 0:
        player_y = 0
    elif player_y >= 945:
        player_y = 945

    # for ships
    for i in range(num_of_ships):
        ship_x[i] += ship_x_change[i]
        if player_state == 1:
            if ship_x[i] <= 0:
                ship_x_change[i] = x
            elif ship_x[i] >= 1074:
                ship_x_change[i] = -x
        else:
            if ship_x[i] <= 0:
                ship_x_change[i] = y
            elif ship_x[i] >= 1074:
                ship_x_change[i] = -y
        collision = is_collision_for_ships(ship_x[i] + 22, ship_y[i] + 33,
                                           player_x,
                                           player_y)
        if collision:
            cnt_time = 900
            collision_ = mixer.Sound("collision.wav")
            collision_.play()
            pygame.time.delay(1000)
            screen.fill((0, 0, 0))
            screen.blit(loss, (350, 480))
            pygame.display.update()
            pygame.time.wait(1000)

            pygame.display.flip()

            if player_state == 2:
                player_x = width / 2 - 20
                player_y = height
                player_state = 1
                for j in range(9):
                    flag[j] = 0
            elif player_state == 1:
                player_x = width / 2 - 20
                player_y = 0
                player_state = 2
                for j in range(9):
                    flag[j] = 0
        if player_y <= 0 and player_state == 1:
            for j in range(9):
                flag[j] = 0
        elif player_y >= 935 and player_state == 2:
            for j in range(9):
                flag[j] = 0
        ship(ship_x[i], ship_y[i], i)

    # for trees
    for i in range(num_of_trees):
        collision2 = is_collision_for_tree(tree_x[i], tree_y[i], player_x,
                                           player_y)
        if collision2:
            cnt_time = 900
            collision_ = mixer.Sound("collision.wav")
            collision_.play()
            pygame.time.delay(1000)
            display_crash_text = True
            screen.fill((0, 0, 0))
            if display_crash_text:
                screen.blit(loss, (350, 480))
            pygame.display.update()
            pygame.time.wait(1000)
            if player_state == 2:
                pygame.display.flip()
                player_x = width / 2 - 20
                player_y = height
                player_state = 1
                for j in range(9):
                    flag[j] = 0
            elif player_state == 1:
                pygame.display.flip()
                player_x = width / 2 - 20
                player_y = 0
                player_state = 2
                for j in range(9):
                    flag[j] = 0
        tree(tree_x[i], tree_y[i], i)

    # for balls
    for i in range(num_of_balls):
        collision2 = is_collision_for_ball(ball_x[i], ball_y[i], player_x,
                                           player_y)
        if collision2:
            collision_ = mixer.Sound("bounce.wav")
            collision_.play()
            if player_y < ball_y[i]:
                player_y -= 30
            else:
                player_y += 30
        ball(ball_x[i], ball_y[i], i)

    # function calls
    if player_y <= 10 and player_state == 1:
        player_x = width / 2 - 20
        player_y = height
        cnt_level[player_state - 1] += 1
        level[player_state - 1] += 1
        score_value += int(cnt_time / 30)
        next_level()
    elif player_y >= 930 and player_state == 2:
        player_x = width / 2 - 20
        player_y = 0
        cnt_level[player_state - 1] += 1
        level[player_state - 1] += 1
        score_value2 += int(cnt_time / 30)
        next_level()
    show_score()
    score(player_y, player_state)
    player(player_x, player_y)
    if cnt_time > 0:
        cnt_time -= 1
    clock.tick(30)
    pygame.display.update()
    pygame.display.flip()
