--Help Page-- 

 
--Gameplay-- 
There are 2 players in the game. 
1st Player starts from the bottom and has to go to top to finish level. 
2nd Player starts from top and has to go to bottom to finish level. 
Turn will go to other player if a player collides with a obstacle(except the beach ball). 
 
--Controls-- 
Both the players can move in any direction with arrow keys. 
Press 'Space' to pause the game as well as to resume the game from where it was paused.

--Obstacles-- 
There are 2 types of obstacles: 
1. Ships: Moving 
2. Palm Trees & Beach ball: Static 

The beach ball will make the player bounce off into the river where a ship can hit it.
Whereas, if the player collides with the ships or trees his turn will be over and the other player will start playing.
 
--Scoring-- 
Each player will start with a score of zero. 
There is a timer for 30 seconds to complete a level. The player will get an extra bonus for completing a level before 30 seconds which is equivalent to the time remaining.
Score will increase by 10 on crossing a ship. 
Score will increase by 5 on crossing the platform on which a palm tree is located.


--Ending Game-- 
The game ends when you click the cross button.
Before ending the game a window will show which player won in that game.

Made By-
	Pratyush Pratap Priyadarshi